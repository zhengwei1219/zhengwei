﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.AbstractFactory
{
   public interface IPhoneProduct
    {
        void Callup();
        void SendMsm();
    }
}
