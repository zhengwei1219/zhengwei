﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.AbstractFactory
{
    class XiaomiRouter : IRouterProduct
    {
        public void OpenWifi()
        {
            Console.WriteLine("XiaomiRouter OpenWifi");
        }
    }
}
