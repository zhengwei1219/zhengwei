﻿using System;

namespace GOF.AbstractFactory
{
  
    /// <summary>
    /// 抽象工厂模式
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IFactory factory = null;

            Console.WriteLine("=========== huawei ===========");
            factory = new Huawei();
            Console.WriteLine("=========== xiaomi ===========");
            factory = new Xiaomi();

           IPhoneProduct phone = factory.CreateIPhone();
           IRouterProduct route =  factory.CreateIRoute();

            phone.Callup();
            route.OpenWifi();






        }
    }
}
