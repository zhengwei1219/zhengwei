﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.AbstractFactory
{
    public class Xiaomi : IFactory
    {
        public IPhoneProduct CreateIPhone()
        {
            return new XiaomiPhone();
        }

        public IRouterProduct CreateIRoute()
        {
            return new XiaomiRouter();
        }
    }
}
