﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.AbstractFactory
{
    public class HuaweiPhone : IPhoneProduct
    {
        public void Callup()
        {
             Console.WriteLine("HuaweiPhone Callup");
        }

        public void SendMsm()
        {
            Console.WriteLine("HuaweiPhone SendMsm");
        }
    }
}
