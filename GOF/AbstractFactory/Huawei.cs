﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.AbstractFactory
{
    public class Huawei : IFactory
    {
        public IPhoneProduct CreateIPhone()
        {
            return new HuaweiPhone();
        }

        public IRouterProduct CreateIRoute()
        {
            return new HuaweiRouter();
        }
    }
}
