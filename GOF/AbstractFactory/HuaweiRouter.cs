﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.AbstractFactory
{
    public class HuaweiRouter : IRouterProduct
    {
        public void OpenWifi()
        {
            Console.WriteLine("HuaweiRouter OpenWifi");
        }
    }
}
