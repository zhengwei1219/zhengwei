﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.AbstractFactory
{
    /// <summary>
    /// 提供了创建产品的接口，它包含多个创建产品的方法，可以创建多个不同等级的产品。
    /// 如果现在想增加一个产品等级，如新加一种笔记本产品，就需要修改接口，因为他只关注产品族。
    /// 比较：
    /// 工厂方法：关注的是产品类型 比喻是工厂中的什么产品，Phone or route or notebook
    /// 抽象工厂：关注的是产品族 比喻是huawei or xiaomi 
    /// </summary>
    public interface IFactory
    {
        IPhoneProduct CreateIPhone();
        IRouterProduct CreateIRoute();
    }
}
