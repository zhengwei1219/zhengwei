﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Strategy.CashSystem
{
    class CashRebate : CashSuper
    {
        public double Rebate { get; set; }
        public CashRebate(double rebate)
        {
            this.Rebate = rebate;
        }

        public override double AcceptCash(double money)
        {
            return money * this.Rebate;
        }
    }
}
