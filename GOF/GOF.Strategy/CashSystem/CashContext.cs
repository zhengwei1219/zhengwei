﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Strategy.CashSystem
{
    class CashContext
    {
        private CashSuper Super { get; set; }

        public CashContext(CashSuper super)
        {
            this.Super = super;
        }
        //简单工厂结合
        public CashContext(string type)
        {
            CashSuper super = null;

            switch (type)
            {
                case "1":
                    super = new CashRebate(0.3);
                    break;
                case "2":
                    super = new CashNormal();
                    break;
                case "3":
                    super = new CashReturn(50, 30);
                    break;
                default:
                    super = new CashNormal();
                    break;
            }
            this.Super = super;
        }

        public double GetResult(double money)
        {
            return this.Super.AcceptCash(money);
        }
    }
}
