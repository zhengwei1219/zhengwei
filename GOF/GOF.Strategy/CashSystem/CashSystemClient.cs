﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Strategy.CashSystem
{
    class CashSystemClient
    {
        /// <summary>
        /// 策略模式
        /// </summary>
        public void Main()
        {
            CashContext context = null;
            string type=string.Empty;
            switch(type)
            {
                case "1":
                   context = new CashContext(new CashRebate(0.3));
                    break;
                case "2":
                    context = new CashContext(new CashNormal());
                    break;
                case "3":
                    context = new CashContext(new CashReturn(50,20));
                    break;
                default:
                    context = new CashContext(new CashNormal());
                    break;
            }
            
            context.GetResult(100);
        }
        /// <summary>
        /// 简单工厂
        /// </summary>
        public void Main01()
        {
            CashSuper super= CashFactory.CreateSuper("1");
            super.AcceptCash(100);
        }

        /// <summary>
        /// 策略模式和简单工厂结合
        /// 比较：结合后客户端只人认识一个类，其它的方式要让客户端认识两个类。
        /// </summary>
        public void Main02()
        {
            CashContext context = new CashContext("1");
            context.GetResult(100);
        }
    }
}
