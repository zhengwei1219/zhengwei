﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Strategy.CashSystem
{
    class CashReturn : CashSuper
    {
        private double MoneyCondition { get; set; }
        private double MoneyReturn { get; set; }
        public CashReturn(double moneyCondition,double moneyReturn)
        {
            this.MoneyCondition = moneyCondition;
            this.MoneyReturn = moneyReturn;
        }
        public override double AcceptCash(double money)
        {
            double result = money;
            if(money > this.MoneyCondition)
            {
                result = money - Math.Floor(money / MoneyCondition) * MoneyReturn;
            }
            return result;

        }
    }
}
