﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Strategy
{
    class ConcreteStrategyA : Strategy
    {
        public override void Algorithm()
        {
            Console.WriteLine("ConcreteStrategyA Algorithm");
        }
    }
}
