﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ChainOfResponsibility
{
    class ConcreteHandlerA : Handler
    {
        public override void HandleRequest(int request)
        {
            Console.WriteLine("ConcreteHandlerA");
            if(request>0 && request <10)
            {
                Console.WriteLine("ConcreteHandlerA HandleRequest");
            }
            else if(NextHandle != null)
            {
                NextHandle.HandleRequest(request);
            }
           
        }
    }
}
