﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ChainOfResponsibility
{
    class ConcreteHandlerC : Handler
    {
        public override void HandleRequest(int request)
        {
            Console.WriteLine("ConcreteHandlerC");
            if (request > 30 && request < 40)
            {
                Console.WriteLine("ConcreteHandlerC HandleRequest");
            }
            else if (NextHandle != null)
            {
                NextHandle.HandleRequest(request);
            }
            
        }
    }
}
