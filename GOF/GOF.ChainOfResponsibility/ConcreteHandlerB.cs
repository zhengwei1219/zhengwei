﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ChainOfResponsibility
{
    class ConcreteHandlerB : Handler
    {
        public override void HandleRequest(int request)
        {
            Console.WriteLine("ConcreteHandlerB");
            if (request > 10 && request < 20)
            {
                Console.WriteLine("ConcreteHandlerB HandleRequest");
            }
            else if (NextHandle != null)
            {
                NextHandle.HandleRequest(request);
            }
           
        }
    }
}
