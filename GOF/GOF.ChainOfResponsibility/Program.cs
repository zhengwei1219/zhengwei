﻿using System;

namespace GOF.ChainOfResponsibility
{
    /// <summary>
    /// 责任链模式
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Handler a = new ConcreteHandlerA();
            Handler b = new ConcreteHandlerB();
            Handler c = new ConcreteHandlerC();
            a.SetNextHandle(b);
            b.SetNextHandle(c);
            a.HandleRequest(35);
            Console.Read();
        }
    }
}
