﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ChainOfResponsibility
{
   abstract class Handler
    {
        public Handler NextHandle { get; set; }
        public void SetNextHandle(Handler handler)
        {
            this.NextHandle = handler;
        }
       public abstract void HandleRequest(int request);
    }
}
