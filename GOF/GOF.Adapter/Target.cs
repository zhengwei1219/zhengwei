﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Adapter
{
    class Target
    {
        /// <summary>
        /// 用户想要的接口，可以是具体的或抽象的类，也可以是接口
        /// </summary>
        public virtual void Request()
        {
            Console.WriteLine("Target Request");
        }
    }
}
