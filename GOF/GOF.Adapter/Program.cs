﻿using System;

namespace GOF.Adapter
{
    /// <summary>
    /// 适配器模式
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Target target = new Adapter();
            target.Request();
        }
    }
}
