﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator
{
   public class ConcreteDecoratorB: Decorator
    {
        public override void Operation()
        {
            base.Operation();
            this.add();
            Console.WriteLine("具体装饰对象B的操作！");
        }
       private void add()
        {
            Console.WriteLine("具体装饰对象B的add方法");
        }
    }
}
