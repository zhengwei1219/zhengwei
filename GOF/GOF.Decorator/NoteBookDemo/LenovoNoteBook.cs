﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.NoteBookDemo
{
    public class LenovoNoteBook : NoteBook
    {
        public override void Print()
        {
            Console.WriteLine("lenovo print");
        }
    }
}
