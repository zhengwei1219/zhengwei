﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.NoteBookDemo
{
    public class NoteBookClient
    {

        public void Main()
        {
            LenovoNoteBook lenovo = new LenovoNoteBook();
            NoteBookSticker sticker = new NoteBookSticker(lenovo);
            NoteBookAccessories acce = new NoteBookAccessories(sticker);
            acce.Print();
        }
    }
}
