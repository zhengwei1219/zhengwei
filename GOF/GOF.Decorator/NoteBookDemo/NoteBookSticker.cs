﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.NoteBookDemo
{
   public class NoteBookSticker:Decorator
    {
        public NoteBookSticker(NoteBook noteBook) : base(noteBook) { }


        public override void Print()
        {
            base.Print();
            StickerPrint();
        }

        private void StickerPrint()
        {
            Console.WriteLine("stickerPrint");
        }
    }
}
