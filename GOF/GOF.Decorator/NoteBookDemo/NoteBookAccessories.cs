﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.NoteBookDemo
{
    public class NoteBookAccessories:Decorator
    {
        public NoteBookAccessories(NoteBook noteBook) : base(noteBook) { }

        public override void Print()
        {
            base.Print();
            Console.WriteLine("AccessoriesPrint");
        }
    }
}
