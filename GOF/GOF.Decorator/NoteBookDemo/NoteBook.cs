﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.NoteBookDemo
{
   public abstract class NoteBook
    {
        public abstract void Print();
    }
}
