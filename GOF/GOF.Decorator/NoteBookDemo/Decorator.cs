﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.NoteBookDemo
{
    public class Decorator : NoteBook
    {
        public Decorator(NoteBook noteBook) {
            this.noteBook = noteBook;
        }
        public NoteBook noteBook { get; set; } 
        public override void Print()
        {
            if(noteBook != null)
            {
                noteBook.Print();
            }
        }
    }
}
