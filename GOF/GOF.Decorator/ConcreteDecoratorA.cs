﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator
{
   public class ConcreteDecoratorA:Decorator
    {
        private string addState { get; set; }
        public override void Operation()
        {
            base.Operation();
            addState = "New State";
            Console.WriteLine("具体装饰对象A的操作");
        }
    }
}
