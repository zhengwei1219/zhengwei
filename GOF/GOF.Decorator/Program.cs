﻿using GOF.Decorator.NoteBookDemo;
using GOF.Decorator.PhoneDemo;
using System;

namespace GOF.Decorator
{
    /// <summary>
    /// 装饰器模式
    /// 定义：指在不改变现有对象结构的情况下，动态地给该对象增加一些职责（即增加其额外功能）的模式，它属于对象结构型模式。
    /// </summary>
    class Program
    {
        //static void Main(string[] args)
        //{
        //    ConcreteComponent c = new ConcreteComponent();
        //    ConcreteDecoratorA a = new ConcreteDecoratorA();
        //    ConcreteDecoratorB b = new ConcreteDecoratorB();
        //    a.SetComponent(c);
        //    b.SetComponent(a);
        //    b.Operation();

        //    Console.ReadLine();
        //}

        /// <summary>
        /// 实例一：
        /// </summary>
        /// <param name="args"></param>
        //static void Main(string[] args)
        //{
        //    ApplePhone apple = new ApplePhone();
        //    PhoneAccessories accessor = new PhoneAccessories();
        //    PhoneSticker sticker = new PhoneSticker();
        //    accessor.SetComponent(apple);
        //    sticker.SetComponent(accessor);
        //    sticker.Print();
        //}


        /// <summary>
        /// 实例二：
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            LenovoNoteBook lenovo = new LenovoNoteBook();
            NoteBookSticker sticker = new NoteBookSticker(lenovo);
            NoteBookAccessories acce = new NoteBookAccessories(sticker);
            acce.Print();
        }

    }
}
