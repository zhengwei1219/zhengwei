﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.HouseDemo
{
    public sealed class MyHouse : House
    {
        public override void Renouvation()
        {
            Console.WriteLine("MyHouse:Renouvation");
        }
    }
}
