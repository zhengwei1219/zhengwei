﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.PhoneDemo
{
   public class PhoneClient
    {

        public void Main()
        {
            ApplePhone apple = new ApplePhone();
            PhoneAccessories accessor = new PhoneAccessories();
            PhoneSticker sticker = new PhoneSticker();
            accessor.SetComponent(apple);
            sticker.SetComponent(accessor);
            sticker.Print();
        }

       
    }
}
