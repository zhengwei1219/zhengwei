﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.PhoneDemo
{
   public class PhoneSticker:Decorator
    {
        public override void Print()
        {
            base.Print();
            StickerPrint();
        }

        private void StickerPrint()
        {
            Console.WriteLine("Phone Sticker");
        }
    }
}
