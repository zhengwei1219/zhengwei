﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.PhoneDemo
{
   public class PhoneAccessories:Decorator
    {
        public override void Print()
        {
            base.Print();
            PhoneAccessoriesPrint();
        }

        private void PhoneAccessoriesPrint()
        {
            Console.WriteLine("phone accessorise print");
        }
    }
}
