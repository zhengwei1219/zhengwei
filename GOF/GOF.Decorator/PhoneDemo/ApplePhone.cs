﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.PhoneDemo
{
    public class ApplePhone : Phone
    {
        public override void Print()
        {
            Console.WriteLine("具体手机的操作");
        }
    }
}
