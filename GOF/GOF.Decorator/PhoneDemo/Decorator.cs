﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.PhoneDemo
{
   public abstract class Decorator:Phone
    {
        protected Phone phone { get; set; }

        public void SetComponent(Phone phone)
        {
            this.phone = phone;
        }

        public override void Print()
        {
            if(phone != null)
            {
                phone.Print();
            }
        }
    }
}
