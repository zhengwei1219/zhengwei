﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Decorator.PhoneDemo
{
   public abstract class Phone
    {
        public abstract void Print();
    }
}
