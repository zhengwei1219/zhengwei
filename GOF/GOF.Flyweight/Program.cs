﻿using System;

namespace GOF.Flyweight
{
    /// <summary>
    /// 享元模式，类似于沲技术
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            int e = 10;
            Flyweight x = new FlyweightFactory().GetFlywgight("X");
            x.Operation(e++);
            Flyweight y = new FlyweightFactory().GetFlywgight("Y");
            y.Operation(e++);
            Flyweight z = new FlyweightFactory().GetFlywgight("Z");
            z.Operation(e++);

            Flyweight unShared = new UnSharedConcreteFlyweight();
            unShared.Operation(e++);
            Console.Read();
        }
    }
}
