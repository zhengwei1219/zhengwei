﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Flyweight
{
    /// <summary>
    /// 
    /// </summary>
    public class FlyweightFactory
    {
        private Hashtable flyweights = new Hashtable();
        public FlyweightFactory(){
            flyweights.Add("X",new ConcreteFlyweight());
            flyweights.Add("Y",new ConcreteFlyweight());
            flyweights.Add("Z",new ConcreteFlyweight());
             }


        public Flyweight GetFlywgight(string key)
        {
            return ((Flyweight)flyweights[key]);
        }
    }
}
