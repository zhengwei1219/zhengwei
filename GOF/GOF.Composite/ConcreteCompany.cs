﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Composite
{
    public class ConcreteCompany : Company
    {
        public ConcreteCompany(string name):base(name)
        {

        }
        public List<Company> companys = new List<Company>();
        public override void Add(Company company)
        {
            companys.Add(company);
        }

        public override void Display(int depth)
        {
            Console.WriteLine(new string('-',depth)+Name);
            companys.ForEach(a => a.Display(depth + 2));
        }

        public override void Remoove(Company company)
        {
            company.Remoove(company);
        }
    }
}
