﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Composite
{
    public class HRDept : Company
    {
        public HRDept(string name) : base(name) { }
        public override void Add(Company company)
        {
            return;
        }

        public override void Display(int depth)
        {
            Console.WriteLine(new string('-', depth) + Name);
        }

        public override void Remoove(Company company)
        {
            return;
        }
    }
}
