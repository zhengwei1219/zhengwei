﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Composite
{
   public abstract class Company
    {
        protected string Name { get; set; }
        public  Company(string name)
        {
            this.Name = name;
        }

        public abstract void Add(Company company);
        public abstract void Remoove(Company company);
        public abstract void Display(int depth);
    }
}
