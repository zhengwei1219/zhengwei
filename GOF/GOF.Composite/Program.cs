﻿using System;

namespace GOF.Composite
{
    class Program
    {
        /// <summary>
        /// 组合模式
        /// 定义：有时又叫作整体-部分（Part-Whole）模式，它是一种将对象组合成树状的层次结构的模式，用来表示“整体-部分”的关系，使用户对单个对象和组合对象具有一致的访问性，属于结构型设计模式。
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            ConcreteCompany root = new ConcreteCompany("北京总部");
            HRDept ahr = new HRDept("人事部");
            FinanceDept afin = new FinanceDept("财务部");

            root.Add(ahr);
            root.Add(afin);
            ConcreteCompany one = new ConcreteCompany("武汉分公司");
            HRDept ahrone = new HRDept("人事部");
            FinanceDept afinone = new FinanceDept("财务部");
            one.Add(ahrone);
            one.Add(afinone);
            root.Add(one);
            ConcreteCompany two = new ConcreteCompany("上海分公司");
            HRDept ahrtwo = new HRDept("人事部");
            FinanceDept afintwo = new FinanceDept("财务部");
            two.Add(ahrtwo);
            two.Add(afintwo);
            root.Add(two);

            root.Display(1);
            Console.Read();

        }
    }
}
