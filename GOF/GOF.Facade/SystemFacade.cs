﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Facade
{
    public class SystemFacade
    {
        SystemOne one { get; set; }
        SystemTwo two { get; set; }
        SystemThree three { get; set; }
        SystemFour four { get; set; }

        public SystemFacade()
        {
            one = new SystemOne();
            two = new SystemTwo();
            three = new SystemThree();
            four = new SystemFour();
        }

        public void MethodA()
        {
            one.MethodOne();
            two.MethodTwo();
        }

        public void MethodB()
        {
            three.MethodThree();
            four.MethodFour();
        }
    }
}
