﻿using System;

namespace GOF.Facade
{
    /// <summary>
    /// 外观模式
    /// 定义：为子系统中的一组接口提供一个一致的界面，此模式定义了一个高层接口
    /// 这一接口使得这一子系统更加容易使用
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            SystemFacade facade = new SystemFacade();
            facade.MethodA();
            facade.MethodB();
        }
    }
}
