﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Mediator
{
   abstract class Colleague
    {
        public Mediator mediator { get; set; }
        public Colleague(Mediator m)
        {
            this.mediator = m;
        }
        public abstract void Send(string msg);
        public abstract void Notify(string msg);
    }
}
