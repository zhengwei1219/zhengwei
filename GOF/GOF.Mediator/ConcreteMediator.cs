﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Mediator
{
    class ConcreteMediator : Mediator
    {
   
        
        public ConcreteMediator()
        {
        }
        public override void Send(string msg, Colleague coll)
        {
            foreach (var item in colleagues)
            {
                if(item != coll)
                {
                    item.Notify(msg);
                }
            }
        }
    }
}
