﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Mediator
{
   abstract class Mediator
    {
        public List<Colleague> colleagues = new List<Colleague>();
        public void SetColleagues(Colleague coll)
        {
            colleagues.Add(coll);
        }
        public abstract void Send(string msg, Colleague coll);
        
    }
}
