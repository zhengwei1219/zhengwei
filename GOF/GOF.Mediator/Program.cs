﻿using System;

namespace GOF.Mediator
{
    /// <summary>
    /// 中介者模式
    /// 定义：给分析对象定义一个语言，并定义该语言的文法表示，再设计一个解析器来解释语言中的句子。也就是说，用编译语言的方式来分析应用中的实例。这种模式实现了文法表达式处理的接口，该接口解释一个特定的上下文。
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Mediator m = new ConcreteMediator();
            Colleague collA = new ConcreteColleagueA(m);
            Colleague collB = new ConcreteColleagueB(m);
            m.SetColleagues(collA);
            m.SetColleagues(collB);
            collA.Send("my name is collA");
            collB.Send("my name is collB");
            Console.Read();
        }
    }
}
