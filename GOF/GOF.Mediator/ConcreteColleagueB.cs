﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Mediator
{
    class ConcreteColleagueB : Colleague
    {
        public ConcreteColleagueB(Mediator m):base(m)
        {
        }
        public override void Send(string msg)
        {
            mediator.Send(msg,this);
        }
        public override void Notify(string msg)
        {
            Console.WriteLine($"ConcreteColleagueB GetNotify:{msg}");
        }
    }
}
