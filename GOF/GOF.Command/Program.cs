﻿using GOF.Command.BreakfastDemo;
using System;

namespace GOF.Command
{
    /// <summary>
    /// 命令模式
    /// 定义：将一个请求封装为一个对象，从而使你可用不同的请求对客户进行参数化，对请求排队或记录请求日志，以及支持可撤销的操作
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            ConcreteCommand command = new ConcreteCommand();
            Invoker invoker = new Invoker(command);
            invoker.Call();
            Console.Read();

            //BreakfastDemo 模拟点餐
            Waiter waiter = new Waiter();
            waiter.ChooseChanfen();
            waiter.ChooseHefen();
            waiter.ChooseHuntun();

            Console.Read();
        }
    }
}
