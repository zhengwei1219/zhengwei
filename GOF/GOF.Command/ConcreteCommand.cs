﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command
{
    /// <summary>
    /// 具体命令
    /// </summary>
    class ConcreteCommand : Command
    {

        /// <summary>
        /// 命令必须有一个接收者
        /// </summary>
        private Receiver receiver { get; set; }
        public ConcreteCommand()
        {
            receiver = new Receiver();
        }
        public override void Execute()
        {
            Console.WriteLine("ConcreteCommand.Execute");
            receiver.Action();
        }
    }
}
