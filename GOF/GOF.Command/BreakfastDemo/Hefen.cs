﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command.BreakfastDemo
{
    class Hefen : Breakfast
    {
        private HefenChef chef;
        public Hefen()
        {
            chef = new HefenChef();
        }
        public override void Cooking()
        {
            Console.WriteLine("Hefen.Cooking");
            chef.DoBreakfast();
        }
    }
}
