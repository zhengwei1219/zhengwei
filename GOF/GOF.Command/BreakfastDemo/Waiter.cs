﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command.BreakfastDemo
{
    class Waiter
    {
        public Hefen hefen;
        public Chanfen chanfen;
        public Huntun huntun;
        public Waiter()
        {
            hefen = new Hefen();
            chanfen = new Chanfen();
            huntun = new Huntun();
        }
        public void ChooseHefen()
        {
            hefen.Cooking();
        }
        public void ChooseChanfen()
        {
            chanfen.Cooking();
        }

        public void ChooseHuntun()
        {
            huntun.Cooking();
        }
    }
}
