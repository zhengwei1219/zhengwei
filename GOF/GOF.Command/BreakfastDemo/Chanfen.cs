﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command.BreakfastDemo
{
    class Chanfen : Breakfast
    {
        public ChanfenChef chef;
        public Chanfen()
        {
            this.chef = new ChanfenChef();
        }
        public override void Cooking()
        {
            Console.WriteLine("Chanfen.Cooking");
            chef.DoBreakfast();
        }
    }
}
