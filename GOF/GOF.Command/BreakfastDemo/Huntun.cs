﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command.BreakfastDemo
{
    class Huntun : Breakfast
    {
        private HuntunChef chef;
        public Huntun()
        {
            chef = new HuntunChef();
        }
        public override void Cooking()
        {
            Console.WriteLine("Huntun.Cooking");
            chef.DoHuntun();
        }
    }
}
