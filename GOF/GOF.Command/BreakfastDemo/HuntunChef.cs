﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command.BreakfastDemo
{
    class HuntunChef
    {
        public void DoHuntun()
        {
            Console.WriteLine("HuntunChef.DoHuntun");
        }
    }
}
