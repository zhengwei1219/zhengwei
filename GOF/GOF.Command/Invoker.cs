﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command
{
    /// <summary>
    /// 调用者
    /// </summary>
    class Invoker
    {
        private Command command;
        public Invoker(Command _command)
        {
            this.command = _command;
        }

        public void Call()
        {
            Console.WriteLine("Invoker.Call");
            command.Execute();
        }
    }
}
