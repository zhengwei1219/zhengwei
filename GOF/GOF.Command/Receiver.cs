﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command
{
    /// <summary>
    /// 命令的接收者
    /// </summary>
    class Receiver
    {
        public void Action()
        {
            Console.WriteLine("Receiver.Action");
        }
    }
}
