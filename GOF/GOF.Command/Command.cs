﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Command
{
    /// <summary>
    /// 抽象命令
    /// </summary>
   abstract class Command
    {
      public  abstract void Execute();
    }
}
