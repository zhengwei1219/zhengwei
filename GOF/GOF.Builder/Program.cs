﻿using System;

namespace GOF.Builder
{
    class Program
    {
        /// <summary>
        /// 建造者模式
        /// 定义：将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //造对象A
            AbstractBuilder A = new ConcreteBuilderA();
            Product pA = new Director().Construct(A).GetResult();
            pA.Show();

            //造对象B
            AbstractBuilder B = new ConcreteBuilderB();
            Product pB = new Director().Construct(B).GetResult();
            pB.Show();

            Console.Read();



        }
    }
}
