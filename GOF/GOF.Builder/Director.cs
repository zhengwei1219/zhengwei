﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Builder
{
   public class Director
    {
        public AbstractBuilder Construct(AbstractBuilder builder)
        {
            builder.BuilderA();
            builder.BuilderB();
            return builder;
        }
    }
}
