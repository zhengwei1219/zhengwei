﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Builder
{
    public class ConcreteBuilderA : AbstractBuilder
    {
        private Product p = new Product();
        public override void BuilderA()
        {
            p.Add("部件A");
        }

        public override void BuilderB()
        {
            p.Add("部件B");
        }

        public override Product GetResult()
        {
            return p;
        }
    }
}
