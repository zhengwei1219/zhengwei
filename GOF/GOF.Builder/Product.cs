﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Builder
{
    /// <summary>
    /// 具体的产品，由多个部件组成
    /// </summary>
   public class Product
    {
        List<string> parts = new List<string>();
        public void Add(string part)
        {
            parts.Add(part);
        }
        /// <summary>
        /// 展示产品
        /// </summary>
        public void Show()
        {
            foreach (var item in parts)
            {
                Console.WriteLine(item);
            }
        }
    }
}
