﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Builder
{
  public abstract class AbstractBuilder
    {
       public abstract void BuilderA();
       public abstract void BuilderB();
        public abstract Product GetResult();

    }
}
