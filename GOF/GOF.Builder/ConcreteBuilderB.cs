﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Builder
{
    public class ConcreteBuilderB : AbstractBuilder
    {
        public Product p = new Product();
        public override void BuilderA()
        {
            p.Add("部件X");
        }

        public override void BuilderB()
        {
            p.Add("部件Y");
        }

        public override Product GetResult()
        {
            return p;
        }
    }
}
