﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Memento
{
   public class Girl
    {
        public string Name { get; set; }
        public Girl(string name)
        {
            this.Name = name;
        }
    }
}
