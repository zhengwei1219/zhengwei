﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Memento
{
    /// <summary>
    /// 发起人，可以创建状态到备忘录，可以恢复到指定的状态。
    /// </summary>
   public class Originator
    {
        public string State { get; set; }

        /// <summary>
        /// 创建状态
        /// </summary>
        /// <returns></returns>
        public Memento CreateMemento()
        {
            return new Memento(this.State);
        }
        /// <summary>
        /// 重置到指定的备忘录
        /// </summary>
        /// <param name="m"></param>
       public void RestoreMemento(Memento m)
        {
            this.State = m.State;
        }
    }
}
