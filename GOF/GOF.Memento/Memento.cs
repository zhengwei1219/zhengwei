﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Memento
{
    /// <summary>
    /// 备忘录，里面有可以存状态的字段
    /// </summary>
   public class Memento
    {
        public string State { get; set; }
        public Memento(string state)
        {
            this.State = state;
        }
    }
}
