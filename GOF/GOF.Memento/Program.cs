﻿using System;

namespace GOF.Memento
{
    /// <summary>
    /// 备忘录模式的主要角色如下。
///发起人（Originator）角色：记录当前时刻的内部状态信息，提供创建备忘录和恢复备忘录数据的功能，实现其他业务功能，它可以访问备忘录里的所有信息。
///备忘录（Memento）角色：负责存储发起人的内部状态，在需要的时候提供这些内部状态给发起人。
///管理者（Caretaker）角色：对备忘录进行管理，提供保存与获取备忘录的功能，但其不能对备忘录的内容进行访问与修改。
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Originator or = new Originator();
            Caretaker manager = new Caretaker();
            or.State = "s0";
            Console.WriteLine("初始状态:" + or.State);
            manager.mememto = or.CreateMemento();
            or.State = "s1";
            Console.WriteLine("新的状态："+or.State);
            or.RestoreMemento(manager.mememto);
            Console.WriteLine("恢复的状态：" + or.State);
            Console.ReadLine(); 
        }
    }
}
