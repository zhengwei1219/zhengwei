﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Proxy
{
    class Proxy : Subject
    {
       public RealSubject Realsub { get; set; }
        public Proxy()
        {
            this.Realsub = this.Realsub ?? new RealSubject();
        }
        public override void Request()
        {
            this.Realsub.Request();
        }
    }
}
