﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Proxy
{
    /// <summary>
    /// 定义了RealSubject 和 Proxy共用接口
    /// </summary>
   abstract  class Subject
    {
        public abstract void Request();
    }
}
