﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ObserverPattern
{
    /// <summary>
    /// 抽像观察者类，得到主题类通知时更新自己
    /// </summary>
   abstract class Observer
    {
        public abstract void Update();
    }
}
