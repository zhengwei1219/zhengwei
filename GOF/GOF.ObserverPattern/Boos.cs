﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ObserverPattern
{
    class Boos : Subject
    {
        List<Observer> observers = new List<Observer>();
        private string Action { get; set; }
        public string SubjectState { get => Action; set => Action=value; }

        public void Add(Observer observer)
        {
            observers.Add(observer);
        }

        public void Notify()
        {
            foreach (var item in observers)
            {
                item.Update();
            }
        }

        public void Remove(Observer observer)
        {
            observers.Remove(observer);
        }
    }
}
