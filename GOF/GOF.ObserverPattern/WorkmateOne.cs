﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ObserverPattern
{
    class WorkmateOne : Observer
    {
        public override void Update()
        {
            Console.WriteLine("WorkmateOne.Update");
        }
    }
}
