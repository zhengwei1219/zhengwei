﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.ObserverPattern
{
   interface  Subject
    {
        void Add(Observer observer);
        void Remove(Observer observer);
        void Notify();
        string SubjectState { get; set; }
    }
}
