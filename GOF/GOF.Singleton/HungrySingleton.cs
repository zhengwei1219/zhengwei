﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Singleton
{
   public class HungrySingleton
    {
        private static readonly HungrySingleton instance = new HungrySingleton();
        private HungrySingleton() { }
        public static HungrySingleton GetInstance()
        {
            return instance;
        }
    }
}
