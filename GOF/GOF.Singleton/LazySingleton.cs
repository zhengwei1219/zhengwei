﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Singleton
{
   public class LazySingleton
    {
        private static  LazySingleton lazySingleton = null;
        private LazySingleton() { }

        public LazySingleton GetLazySingleton()
        {
            if(lazySingleton == null)
            {
                lazySingleton = new LazySingleton();
            }
            return lazySingleton;
        }
    }
}
