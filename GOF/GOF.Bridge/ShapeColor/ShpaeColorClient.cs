﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.ShapeColor
{
    /// <summary>
    /// 画图时可以画正方形、长方形、圆形三种形状，而每种形状又可以画红色、黄色、黑色三种颜
    /// </summary>
    class ShpaeColorClient
    {
        public void Main()
        {
            Shape shape = new RectangleShape();
            shape.SetColor(new RedColor());
            shape.Draw();
        }
    }
}
