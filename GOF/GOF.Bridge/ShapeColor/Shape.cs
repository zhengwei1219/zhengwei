﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.ShapeColor
{
   abstract class Shape
    {
        public Color color { get; set; }
        public void SetColor(Color c)
        {
            this.color = c;
        }
        public abstract void Draw();
    }
}
