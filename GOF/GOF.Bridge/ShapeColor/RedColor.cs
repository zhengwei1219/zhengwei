﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.ShapeColor
{
    class RedColor : Color
    {
        public override void Bepaint()
        {
            Console.WriteLine("Red");
        }
    }
}
