﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.ShapeColor
{
    class RectangleShape : Shape
    {
        public override void Draw()
        {
            color.Bepaint();
            Console.WriteLine("RectangleShape Draw");
        }
    }
}
