﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.RoadCar
{
    class RoadCarClient
    {
        public void Main() {
            
            AbstractRoad road = new SpeedWay();
            road.SetCar(new Bus());
            road.Run();


            AbstractRoad roadB = new StreetWay();
            roadB.SetCar(new MiniCar());
            roadB.Run();
            Console.Read();
        }
        
        
    }
}
