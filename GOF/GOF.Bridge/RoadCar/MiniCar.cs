﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.RoadCar
{
    class MiniCar : AbatractCar
    {
        public override void Run()
        {
            Console.WriteLine("MiniCar Run");
        }
    }
}
