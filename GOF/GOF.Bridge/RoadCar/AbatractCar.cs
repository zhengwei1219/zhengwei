﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.RoadCar
{
   abstract class AbatractCar
    {
        public abstract void Run();
    }
}
