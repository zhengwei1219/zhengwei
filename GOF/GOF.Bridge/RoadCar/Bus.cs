﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.RoadCar
{
    class Bus : AbatractCar
    {
        public override void Run()
        {
            Console.WriteLine("Bus Run");
        }
    }
}
