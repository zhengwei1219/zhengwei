﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.RoadCar
{
   abstract class AbstractRoad
    {
        public AbatractCar Car { get; set; }
        public abstract void Run();
        public void SetCar(AbatractCar car)
        {
            this.Car = car;
        }
    }
}
