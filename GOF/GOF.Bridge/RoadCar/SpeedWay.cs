﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge.RoadCar
{
    class SpeedWay : AbstractRoad
    {
        public override void Run()
        {
            this.Car.Run();
            Console.WriteLine("SpeedWay Run");
        }
    }
}
