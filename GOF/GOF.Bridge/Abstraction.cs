﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge
{
    class Abstraction
    {
        protected Implementor imp;
     

        public void SetImplementor(Implementor impl)
        {
            this.imp = impl;
        }
        public virtual void Operation()
        {
            this.imp.Operation();
        }
    }
}
