﻿using GOF.Bridge.RoadCar;
using GOF.Bridge.ShapeColor;
using System;

namespace GOF.Bridge
{
    /// <summary>
    /// 桥接模式
    /// 定义：将抽象与实现分离，使它们可以独立变化。它是用组合关系代替继承关系来实现，从而降低了抽象和实现这两个可变维度的耦合度。
    /// </summary>
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Abstraction ab = new RefinedAbstraction();
        //    ab.SetImplementor(new ConcreteImplementorA());
        //    ab.Operation();

        //    ab.SetImplementor(new ConcreteImplementorB());
        //    ab.Operation();

        //    Console.Read();
        //}

        /// <summary>
        /// 实例一：
        /// </summary>
        /// <param name="args"></param>
        //static void Main(string[] args)
        //{

        //    AbstractRoad road = new SpeedWay();
        //    road.SetCar(new Bus());
        //    road.Run();


        //    AbstractRoad roadB = new StreetWay();
        //    roadB.SetCar(new MiniCar());
        //    roadB.Run();
        //    Console.Read();
        //}
        static void Main(string[] args)
        {
            Shape shape = new RectangleShape();
            shape.SetColor(new RedColor());
            shape.Draw();
            Console.Read();
        }

    }
}
