﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Bridge
{
    class RefinedAbstraction:Abstraction
    {
        public override void Operation()
        {
            this.imp.Operation();
            Console.WriteLine("RefinedAbstraction Operation");
        }
    }
}
