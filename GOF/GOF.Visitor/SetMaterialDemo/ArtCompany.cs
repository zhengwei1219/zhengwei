﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor.SetMaterialDemo
{
    /// <summary>
    /// 艺术公司根据铜造铜像，根据纸造画像
    /// </summary>
    class ArtCompany : Company
    {
        public override string CreateByCuprum(Cuprum cuprum)
        {
           return "艺术公司的铜像";
        }

        public override string CreateByPaper(Paper paper)
        {
            return "艺术公司的画像";
        }
    }
}
