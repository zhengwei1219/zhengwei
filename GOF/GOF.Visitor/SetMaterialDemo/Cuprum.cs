﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor.SetMaterialDemo
{
    class Cuprum : Material
    {
        public override void Accept(Company company)
        {
            Console.WriteLine($"用铜创造出来的:{company.CreateByCuprum(this)}");
        }
    }
}
