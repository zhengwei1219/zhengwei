﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor.SetMaterialDemo
{
    
    class SetMaterial
    {
        IList<Material> materials = new List<Material>();

        public void Add(Material material)
        {
            materials.Add(material);
        }
        public void Remove(Material material)
        {
            materials.Remove(material);
        }

        public void Accept(Company company)
        {
            foreach (var item in materials)
            {
                item.Accept(company);
            }
        }
    }
}
