﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor.SetMaterialDemo
{
    class Paper : Material
    {
        public override void Accept(Company c)
        {
            Console.WriteLine($"用纸创出来的:{c.CreateByPaper(this)}");
        }
    }
}
