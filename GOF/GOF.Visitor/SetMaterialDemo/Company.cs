﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor.SetMaterialDemo
{
    /// <summary>
    /// 提供了根据铜和纸创建东西的方法
    /// </summary>
    abstract class Company
    {
        public abstract string CreateByPaper(Paper paper);
        public abstract string CreateByCuprum(Cuprum cuprum);
    }
}
