﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor.SetMaterialDemo
{
    /// <summary>
    /// 造币类公司
    /// </summary>
    class MintCompany : Company
    {
        public override string CreateByCuprum(Cuprum cuprum)
        {
            return "造币公司造出了铜币";
        }

        public override string CreateByPaper(Paper paper)
        {
           return "造纸公司造出了纸币";
        }
    }
}
