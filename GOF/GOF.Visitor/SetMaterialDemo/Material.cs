﻿using GOF.Visitor.SetMaterialDemo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor
{
    /// <summary>
    /// 材料类
    /// </summary>
   abstract class Material
    {
        public abstract void Accept(Company c);
    }
}
