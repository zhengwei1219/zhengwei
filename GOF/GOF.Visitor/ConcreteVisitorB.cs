﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor
{
    class ConcreteVisitorB : Visitor
    {
        public override void VisitElementA(ElementA e)
        {
            Console.WriteLine("ConcreteVisitorB.VisitElementA");
        }

        public override void VisitElementB(ElementB e)
        {

            Console.WriteLine("ConcreteVisitorB.VisitElementB");
        }
    }
}
