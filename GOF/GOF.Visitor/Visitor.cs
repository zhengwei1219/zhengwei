﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor
{
   abstract class Visitor
    {
        public abstract void VisitElementA(ElementA e);
        public abstract void VisitElementB(ElementB e);
    }
}
