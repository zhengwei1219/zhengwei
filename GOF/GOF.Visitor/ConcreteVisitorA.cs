﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.Visitor
{
    class ConcreteVisitorA : Visitor
    {
        public override void VisitElementA(ElementA e)
        {
            Console.WriteLine("ConcreteVisitorA.VisitElementA");
        }

        public override void VisitElementB(ElementB b)
        {
            Console.WriteLine("ConcreteVisitorA.VisitElementB");
        }
    }
}
