﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.SimpleFactory
{
    class OperationFactory
    {
        public static Operation CreateOperation(string type)
        {
            Operation p = null;
            switch (type)
            {
                case "+":
                    p = new AddOperation();
                    break;
                case "-":
                    p = new SubOperation();
                    break;
                case "*":
                    p = new MulOperation();
                    break;
                case "/":
                    p = new DivOperation();
                    break;
                default:
                    break;
            }
            return p;
        }
    }
}
