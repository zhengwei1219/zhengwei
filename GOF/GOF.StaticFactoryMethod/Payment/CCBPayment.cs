﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.SimpleFactory.Payment
{
    public class CCBPayment : IPayment
    {
        public bool Payfor(decimal money)
        {
            Console.WriteLine($"CCB pay success:{money}");
            return true;
        }
    }
}
