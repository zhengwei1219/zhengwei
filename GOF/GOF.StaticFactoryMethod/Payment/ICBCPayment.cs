﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.SimpleFactory.Payment
{
    public class ICBCPayment : IPayment
    {
        public bool Payfor(decimal money)
        {
            Console.WriteLine($"ICBC支付成功：{money}");
            return true;
        }
    }
}
