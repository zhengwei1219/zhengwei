﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.SimpleFactory.Payment
{
    public static class PayFactory
    {
        public static IPayment CreatePayment(string payType)
        {
            IPayment payment = null;
            switch (payType)
            {
                case "ICBC":
                    payment = new ICBCPayment();
                    break;
                case "CCB":
                    payment=new CCBPayment();
                    break;
                default:
                    payment = new CCBPayment();
                    break;
            }
            return payment;
        }
    }
}
