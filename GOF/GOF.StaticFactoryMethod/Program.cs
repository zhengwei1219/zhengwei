﻿using GOF.SimpleFactory.Payment;
using System;

namespace GOF.SimpleFactory
{
    /// <summary>
    /// 简单工厂模式
    /// 定义：是对简单工厂模式的进一步抽象化，其好处是可以使系统在不修改原来代码的情况下引进新的产品，即满足开闭原则。
    /// </summary>
    class Program
    {
        /// <summary>
        /// 简单工厂模式实现一：不同的符号实现不同的算法
        /// </summary>
        /// <param name="args"></param>
        //static void Main(string[] args)
        //{
        //    Operation p = OperationFactory.CreateOperation("-");
        //    p.NumberA = 1;
        //    p.NumberB = 2;
        //   double result = p.GetResult();
        //    Console.WriteLine("结果："+result);
        //    Console.ReadLine();
        //}

        /// <summary>
        /// 简单工厂方法模式二：不同的银行实现不同的支付接口
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            IPayment pay = PayFactory.CreatePayment("CCB");
             pay.Payfor(100);
            Console.ReadLine() ;
        }
    }
}
