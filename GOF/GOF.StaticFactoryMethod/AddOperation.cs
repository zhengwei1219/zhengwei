﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.SimpleFactory
{
   public class AddOperation:Operation
    {
        public override double GetResult()
        {
            return NumberA + NumberB;
        }
    }
}
