﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod
{
    abstract class AbstarctClass
    {
        public abstract void AbstarctMethodA();
        public abstract void AbstarctMethodB();
        //定义的算法的骨架，抽象的方法推迟到子类去执行。
        public void TemplateMethod()
        {
            AbstarctMethodA();
            AbstarctMethodB();
            Console.WriteLine("TemplateMethod");
        }
    }
}
