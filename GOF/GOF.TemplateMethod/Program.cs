﻿using GOF.TemplateMethod.AccountDemo;
using GOF.TemplateMethod.ReportPrintDemo;
using System;


namespace GOF.TemplateMethod
{
    /// <summary>
    /// 模版方法模式
    /// 定义：定义一个操作中的算法骨架，而将一些步骤推迟到子类中去，可以不改变一个算法的骨架结构即可以重新定义该算法的某些特定的步骤。
    ///优点：
    ///在父类中形式化的定义了一个算法，而又他的子类去处理实现的细节，在子类中实现详细的处理算法时并不会改变算法中步骤的执行顺序。
    ///模板方法是一种代码复用技术，在类库设计中尤为重要，他提取类库中的公共行为，，并通过子类来实现不同行为。鼓励适当地使用继承来实现代码复用。
    ///缺点：
    ///需要为每一种基本方法的不同实现提供一个子类，这将会导致系统的类的个数快速增加，可通过桥接模式来优化。
    /// </summary>
    class Program
    {
        /// <summary>
        /// 按定义实现
        /// </summary>
        /// <param name="args"></param>
        //static void Main(string[] args)
        //{
        //    AbstarctClass A = new ConcreteClassA();
        //    A.TemplateMethod();

        //    AbstarctClass B = new ConcreteClassB();
        //    B.TemplateMethod();

        //    Console.ReadLine();
        //}
        /// <summary>
        /// 模版方法模式二：实现不同类型的报表打印显示不同
        /// </summary>
        /// <param name="args"></param>
        //static void Main(string[] args)
        //{
        //    ReportPrint reportPrintA = new ReportPrintA();
        //    reportPrintA.Print();


        //    ReportPrint reportPrintB = new ReportPrintB();
        //    reportPrintB.Print();

        //    Console.ReadLine();
        //}

        /// <summary>
        /// 模版方法模式三：
        /// 例子：某软件公司要为某银行的业务支撑系统开发一个利息计算模块，计算流程如下：
        ///系统根据账号和密码验证用户信息，如果用户信息错误，则系统显示出错信息。
        ///如果信息正确，则更具用户类型的不同使用不同的利息计算公式计算利息。系统显示利息。
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
           Account accountA = new AccountSaving("zhengwei", "123");
            accountA.Execute();

            Account accountB = new AccountCredit("liudehua", "123");
            accountB.Execute();



            Console.ReadLine();
        }

       

    }
}
