﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod.ReportPrintDemo
{
   public abstract class ReportPrint
    {
        public void PrintTitle()
        {
            Console.WriteLine("ReportPrint PrintTitle");

        }
        public abstract void PrintBody();
        public void PrintTail() {
            Console.WriteLine("ReportPrint PrintTail");
        }
        public void Print()
        {
            PrintTitle();
            PrintBody();
            PrintTail();
        }
    }
}
