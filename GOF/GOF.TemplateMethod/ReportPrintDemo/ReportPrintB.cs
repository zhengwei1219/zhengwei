﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod.ReportPrintDemo
{
    class ReportPrintB : ReportPrint
    {
        public override void PrintBody()
        {
            Console.WriteLine("ReportPrintB PrintBody");
        }
    }
}
