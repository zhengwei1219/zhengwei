﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod.AccountDemo
{
    public abstract class Account
    {
        public Account() { }
        public string UserName { get; set; }
        public string Password { get; set; }    

        public Account(string username, string password)
        {
            UserName = username;
            Password = password;
        }
      
        public  void Execute()
        {
            if(Check())
            {
                Calculate(this.UserName,this.Password);
            }
        }

        public abstract decimal Calculate(string username,string password);

        public bool Check()
        {
            var userName = UserName;
            var password = Password;
            if(!string.IsNullOrEmpty(userName) &&  !string.IsNullOrEmpty(password))
            {
                //数据库正确性校验
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }
}
