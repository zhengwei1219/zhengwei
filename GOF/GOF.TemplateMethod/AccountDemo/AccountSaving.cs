﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod.AccountDemo
{
    public class AccountSaving : Account
    {
        public AccountSaving(string username, string password):base(username,password) { }
        public override decimal Calculate(string username, string password)
        {
            //根据用户名进行计算
            if (username == "Liudehua")
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
