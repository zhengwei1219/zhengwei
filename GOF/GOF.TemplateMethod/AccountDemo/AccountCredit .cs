﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod.AccountDemo
{
    public class AccountCredit : Account
    {
        public AccountCredit(string username, string password) : base(username,password) { }
        public override decimal Calculate(string username, string password)
        {
            if (username == "Zhengwei")
            {
                return 100;
            }
            else
            {
                return 0;
            }
        }
    }
}
