﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod
{
    class ConcreteClassA : AbstarctClass
    {
        public override void AbstarctMethodA()
        {
            Console.WriteLine("ConcreteClassA AbstarctMethodA");
        }

        public override void AbstarctMethodB()
        {
            Console.WriteLine("ConcreteClassA AbstarctMethodB");
        }
    }
}
