﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOF.TemplateMethod
{
    class ConcreteClassB : AbstarctClass
    {
        public override void AbstarctMethodA()
        {
            Console.WriteLine("ConcreteClassB AbstarctMethodA");
        }

        public override void AbstarctMethodB()
        {
            Console.WriteLine("ConcreteClassB AbstarctMethodB");
        }
    }
}
