﻿using System;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {

           var type = typeof(MyClass<>).MakeGenericType(typeof(Dog));

        }
    }

    class MyClass<T>
    {
        public void GetTName()
        {
            Console.WriteLine($"----:{typeof(T).Name}"); 
        }
    }

    class Dog
    {

    }
}
