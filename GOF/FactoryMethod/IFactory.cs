﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    /// <summary>
    /// 提供了创建产品的接口，调用者通过它访问具体工厂的工厂方法来创建产品。
    /// </summary>
    interface IFactory
    {
        Operation CreateOperation();
    }
}
