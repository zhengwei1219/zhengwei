﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class DivOperation : Operation
    {
        public override double GetResult()
        {
            if (NumberB == 0)
            {
                throw new Exception("NumberB  is zero");
            }
            return NumberA / NumberB;
        }
    }
}
