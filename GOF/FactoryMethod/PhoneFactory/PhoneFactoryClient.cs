﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.PhoneFactory
{
   
    class PhoneFactoryClient
    {

        public void Main()
        {
            //工厂方法模式主要的角色：抽象工厂、具体工厂、抽象产品、具体产品
            //比较：抽象工厂的主要角色和工厂方法的主要角色是一样的，也是以上四种
            IProduct product = null;
            IFactory factory = null;


            factory = new Huawei();
            factory = new Xiaomi();

            product = factory.CreateProduct();

            product.Show();

        }
      

    }
}
