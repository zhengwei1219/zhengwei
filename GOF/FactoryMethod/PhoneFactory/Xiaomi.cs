﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.PhoneFactory
{
    class Xiaomi : IFactory
    {
        public IProduct CreateProduct()
        {
            return new Route();
        }
    }
}
