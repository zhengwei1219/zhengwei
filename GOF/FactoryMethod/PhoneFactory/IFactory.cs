﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.PhoneFactory
{
    /// <summary>
    /// 只关注产品等级，不关注产品族请与抽象工厂对比
    /// </summary>
    interface IFactory
    {
        IProduct CreateProduct();
    }
}
