﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.PhoneFactory
{
    class Route : IProduct
    {
        public void Show()
        {
            Console.WriteLine("Route Show");
        }
    }
}
