﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.PhoneFactory
{
    class Huawei : IFactory
    {
        public IProduct CreateProduct()
        {
            return new Phone();
        }
    }
}
