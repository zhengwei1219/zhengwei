﻿using System;

namespace FactoryMethod
{
    /// <summary>
    /// 工厂方法模式
    /// 定义：
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IFactory factory = null;
            Operation operation = null;


               factory = new AddFactory();
            factory = new SubFactory();
            factory = new DivFactory();

            factory.CreateOperation();
            operation = factory.CreateOperation();
            operation.NumberA = 1;
            operation.NumberB = 2;
            double result = operation.GetResult();
            Console.WriteLine("结果：" + result);
            Console.ReadLine();
        }
    }
}
