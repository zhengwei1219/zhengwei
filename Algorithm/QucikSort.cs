﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm
{
   public class QucikSort
    {
        public static void Sort(int[] arry,int start,int end)
        {
            if(start<end)
            {
                int mid = Partition(arry, start, end);
                Sort(arry, start, mid-1);
                Sort(arry, mid + 1, end);
            }
        }

        private static int Partition(int[] array, int start, int end)
        {
            int x = array[end];//选一个判定值，一般选最后一个
            int i = start;
            for (int j = start; j < end; j++)
            {
                //如果当前比较的数小于判定值，则将这个值移到判定值的前面
                if(array[j]<x)
                {
                    int temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                    i++;
                }
            }
            array[end] = array[i];
            array[i] = x;
            return i;
        }
    }
}
