﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm
{
    class InsertionSort
    {
        public static void Sort(int[] arry)
        {
            for (int i = 1; i < arry.Length; i++)
            {
                //如果自己比前一位数大，则不再比较，因为当前比较的数的前面都是有序的。
                if (arry[i] > arry[i - 1]) continue;

                int temp = arry[i];
                int j = i;
                //如果当前数小于前一位数，则前一位数后移，再比较向前比较，直到比较到第一位数。
                while(j>0&& temp < arry[j - 1])
                {
                    
                    arry[j] = arry[j - 1];
                    j--;
                }
                //数据后移后留出了一个坑，将当前比较的数填到这个坑中
                arry[j] = temp;
            }
            Console.WriteLine("插入排序："+string.Join(",", arry));
        }
    }
}
