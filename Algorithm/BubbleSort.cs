﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm
{
    class BubbleSort
    {
        public static void Sort(int[] arry)
        {
            int temp = 0;
            for (int i = 0; i < arry.Length; i++)
            {
                for (int j = 0; j < arry.Length-1; j++)
                {
                    //如果前一位数比后一位数大，则交换位置
                    if(arry[j]>arry[j+1])
                    {
                        temp = arry[j];
                        arry[j] = arry[j+1];
                        arry[j+1] = temp;
                    }
                }
            }
            Console.WriteLine("冒泡排序："+string.Join(",",arry));
        }
    }
}
