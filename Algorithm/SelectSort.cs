﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm
{
    class SelectSort
    {
        public static void Sort(int[] arry)
        {
            for (int i = 0; i < arry.Length; i++)
            {
                int min = arry[i];
                int minIndex = i;
                //先找到最小的数，注意j是从i+1开始，而不是0
                for (int j = i+1; j < arry.Length; j++)
                {
                    if(min>arry[j])
                    {
                        min = arry[j];
                        minIndex = j;
                    }
                }
                //如果找到了比自己小的数，则交换
                if(minIndex != i)
                {
                    arry[minIndex] = arry[i];
                    arry[i] = min;
                }
            }
            Console.WriteLine("选择排序：" + string.Join(",", arry));
        }
    }
}
