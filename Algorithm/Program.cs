﻿using System;

namespace Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            //插入排序
            int[] arry = { 1, 3, 9, 2, 5, 7, 0 };
            InsertionSort.Sort(arry);

            //冒泡排序
            int[] arry1 = { 1, 3, 9, 2, 5, 7, 0 };
          BubbleSort.Sort(arry1);

            //选择排序
            int[] arry2 = { 1, 3, 9, 2, 5, 7, 0 };
            SelectSort.Sort(arry2);

            //归并排序
            int[] arry3 = { 10, 31, 91, 21, 51, 71, 0 };
            int[] temp = new int[arry.Length];
            MergeSort.Sort(arry3, 0, arry.Length - 1, temp);
            Console.WriteLine("归并排序：" + string.Join(",", arry3));

            //快整排序
            int[] arry4 = { 1, 3, 9, 2, 5, 7, 0 };
            QucikSort.Sort(arry4, 0, 6);
            Console.WriteLine("快速排序：" + string.Join(",", arry4));

        }
    }
}
