﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm
{
   public class MergeSort
    {
        public static void Sort(int[] arr,int left,int right,int[] temp)
        {
            if(left<right)
            {
                int mid = (left + right) / 2;
                Sort(arr, left, mid, temp);
                Sort(arr, mid + 1, right, temp);
                Merge(arr, left, mid, right, temp);
            }
           
        }
        /// <summary>
        /// 归并
        /// </summary>
        /// <param name="arr">原始数组</param>
        /// <param name="left">左边有序序列初始索引</param>
        /// <param name="mid">中间索引</param>
        /// <param name="right">右边有序序列初始索引</param>
        /// <param name="temp">中转数组</param>
        private static void Merge(int[] arr, int left, int mid, int right, int[] temp)
        {
            int i = left;
            int j = mid + 1;
            int t = 0;
            while(i<=mid && j<=right)
            {
                //如果左边的有序序列的当前元素小于等于右边的有序序列的当前元素，则将左边的当前元素拷贝到temp数组
                if(arr[i]<=arr[j])
                {
                    temp[t] = arr[i];
                    t+=1;
                    i+=1;
                }
                else
                {
                    //否则，将右边的当前元素拷贝到temp数组
                    temp[t] = arr[j];
                    t+=1;
                    j+=1;
                }
            }

            //左边有序序列剩余的元素全部填充到temp
            while(i<=mid)
            {
                temp[t] = arr[i];
                t+=1;
                i+=1;
            }

            //右边的有序序列剩余元素全部填充到temp
            while(j<=right)
            {
                temp[t] = arr[j];
                t+=1;
                j+=1;
            }

            //将temp数组的元素拷贝到arr
            t = 0;
            int tempLeft = left;
            while(tempLeft <=right)
            {
                arr[tempLeft] = temp[t];
                t+=1;
                tempLeft+=1;
            }

        }
    }
}
